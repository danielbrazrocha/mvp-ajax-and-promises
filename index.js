let globalIsEditing = false;
let globalCurrentItem = null;
var globalNames = null;
let position = null;

let tabMessages = null;
let submitForm = null;
let submitName = null;
let submitMsg = null;
let submitFilterById = null;
let inputToFilteredById = null;

window.addEventListener("load", start);

tabMessages = document.querySelector("#allMessages");
submitForm = document.querySelector("#btnEnviar");
submitFilterById = document.querySelector("#filterById");



async function doFetchAsync() {
  const res = await fetch("https://treinamentoajax.herokuapp.com/messages");
  const json = await res.json();
  globalNames = json;
  render();
}

function preventFormSubmit() {
  function handleSubmit(event) {
    event.preventDefault();
  }

  var form = document.querySelector("form");
  submitFilterById.addEventListener("click", filterMsgById);
  submitForm.addEventListener("click", handleInput);
  form.addEventListener("submit", handleSubmit);
}

async function handleInput() {
  submitName = document.querySelector("#inputName");
  submitMsg = document.querySelector("#inputMsg");

  if (!submitName.value || !submitMsg.value) {
    window.alert("Os campos Nome e Mensagem precisar conter informações");
  } else {
    const fetchBody = {
      message: {
        name: `${submitName.value}`,
        message: `${submitMsg.value}`,
      },
    };
    const fetchConfig = {
      method: "POST",
      headers: { "Content-Type": "application/JSON" },
      body: JSON.stringify(fetchBody),
    };

    try {
      await fetch(
        "https://treinamentoajax.herokuapp.com/messages",
        fetchConfig
      );
      console.log(
        `Sucesso! Submitting Nome:${submitName.value} e Msg:${submitMsg.value}`
      );
      doFetchAsync();
    } catch (error) {}
  }
}

function start() {
  doFetchAsync();
  preventFormSubmit();
  //activateInput();
}

function activateInput() {
  function handleKeyup(event) {
    /**
     * Enquanto o usuário não digital Enter,
     * nada será feito
     */
    if (event.key !== "Enter") {
      return;
    }

    /**
     * Obtendo o valor digitado
     * trim() elimina os espaços em
     * branco no início e no final da string
     */
    var currentName = inputName.value.trim();

    /**
     * Verificando se o valor é passível
     * de ser um nome
     */
    if (currentName === "") {
      clear();
      return;
    }

    /**
     * Identificando se estamos inserindo ou
     * editando algum nome
     */
    if (globalIsEditing) {
      /**
       * Para editar, trocamos o valor do
       * item do vetor a partir de
       * globalCurrentItem
       */
      globalNames[globalCurrentItem] = currentName;
    }
    /**
     * Considerando o texto da posição somente se ele
     * for um valor menor que o comprimento do array de nomes
     */
    console.log(globalIsEditing);
    if (!globalIsEditing && inputPos.value && inputPos.value <= globalNames) {
      globalNames.splice(inputPos.value - 1, 0, currentName);
    }
    /**
     * Para inserir na ultima posição, basta invocar o
     * método push
     */
    if (!globalIsEditing && !inputPos.value) {
      globalNames.push(currentName);
    } else {
    }

    /**
     * Limpando o formulário e
     * renderizando os dados
     */
    clear();
    renderNames();
  }

  let inputName = getInput();
  let inputPos = getPosInput();
  inputName.addEventListener("keyup", handleKeyup);
  inputPos.addEventListener("keyup", handleKeyup);
}

/**
 * Em vários momentos isso foi
 * necessário. Portanto, é uma
 * boa prática isolar o comando
 * em uma função
 */
function getInput() {
  return document.querySelector("#inputName");
}

function getPosInput() {
  return document.querySelector("#posNumber");
}

/**
 * Limpa os dados do formulário
 * e "reinicializa" a edição
 */
function clear() {
  var inputName = getInput();
  var inputPos = getPosInput();
  inputPos.value = "";
  inputName.value = "";
  inputName.focus();
  globalIsEditing = false;
}

function render() {
  renderMessages();
}

function renderMessages() {
  let messagesHTML = "<div>";

  if (!globalNames.length) {
    const messageHTML = `
      <div class='messageCard'>
        <div class='cardHeader'>
          <span class='cardSpanName'>${globalNames.name}</span>
          <a id="${globalNames.id}" href="javascript:deleteMsg(${globalNames.id});" class="buttonCard">Deletar</a>
        </div>
        <div class='cardMessage'>
          <p>${globalNames.message}</p>
        </div>        
      </div> 
    `;
    messagesHTML += messageHTML;
  }

  else {    
    globalNames.forEach((message) => {
      const messageHTML = `
      <div class='messageCard'>
      <div class='cardHeader'>
      <span class='cardSpanName'>${message.name}</span>
      <a id="${message.id}" href="javascript:deleteMsg(${message.id});" class="buttonCard">Deletar</a>
      </div>
      <div class='cardMessage'>
      <p>${message.message}</p>
      </div>        
      </div> 
      `;
      messagesHTML += messageHTML;
    });
  }
  messagesHTML += "</div>";
  tabMessages.innerHTML = messagesHTML;
}

async function deleteMsg(id) {
  try {
    await fetch(`https://treinamentoajax.herokuapp.com/messages/${id}`, {
      method: "DELETE",
    }).then(console.log);
    console.log(`Sucesso! Deletado Evento ID nº:${id}`);
    doFetchAsync();
  } catch (err) {console.log(err)}
}



async function filterMsgById() {
  console.log("Filtrando por id")
  inputToFilteredById = document.querySelector("#searchIndexNumber");

  try {
    const res = await fetch(`https://treinamentoajax.herokuapp.com/messages/${inputToFilteredById.value}`);
    const json = await res.json();
    globalNames = json;
    console.log(globalNames);
    render();
    console.log(`Sucesso! Filtrado por ID nº:${inputToFilteredById.value}`);    
  } catch (err) {}
}
